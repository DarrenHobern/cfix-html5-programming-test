## Documentation for Breakout game
---
And other notes about Typescript/Phaser/Nodejs/Javascript

## Phaser Cheatsheet
https://gist.github.com/woubuc/6ef002051aeef453a95b

---

### Creating class that extends sprite:
https://github.com/JamesSkemp/phaser-starter-templates/blob/master/v2-ce/_starter-v2-ce-vsc-ts/src/Prefabs/_ExampleSprite.ts


### Virtual Buttons for mobile control
https://phaser.io/examples/v2/input/virtual-gamecontroller

### Console
`console.log()` outputs to the browser console not the command line where `npm start` is.

### Anonymous functions
Don't have access to class fields, can achieve a similar
result by using lambda functions.
Eg. `function() {this.left = true;}` becomes `() => {this.left = true;}`

### Get dimensions of image before loading it
http://www.html5gamedevs.com/topic/10212-how-to-get-image-dimensions-before-loading-it/?do=findComment&comment=60048

### Scale game to device
https://www.joshmorony.com/how-to-scale-a-game-for-all-device-sizes-in-phaser/

---

#### Some closing thoughts:
This was my first time using Phaser, Typescript, Nodejs, or Javascript, it was quite the learning experience. Initially I had some trouble knowing where to look for debugging,
was it an issue with phaser or Typescript or node? or somethign else? Additionally I was expecting the `console.log()` output to appear in the terminal window running the server. This was not the case, I later found it in the browser console.
Once I got the hang of phaser/typescript the next major issue I found was class functions/constructors and fields. These did not behave in the way I was used to or expected.
In another language like C#, I would extract all the initialisation for the different objects (ball, paddle, boxes, buttons) into their own functions, and then call those from `create()`.
Instead it appears that Typescript doesn't like calling class functions from the constructor.
It also appeared that class fields are initialised after the constructor, meaning that setting constant values was more difficult. I am very interested in seeing the proper way to do what I wanted, because I'm sure its possible.

I spent a bit more time on this then the allotted 6 hours so that I would be able to implement the core features:
* Ball bouncing off paddle + walls + boxes
* Game over if ball goes off the bottom
* Able to restart the game without refreshing
* Scale the game to fit screen size.

Some other features I wanted to implement given more time:
* Different boxes with different health - most of the code for this is done
* Auto adjust for changing screen size/rotation
* Lives and launching the ball from the paddle

Overall this was a fun little project and a good introduction to Phaser and Typescript.
I enjoyed the lightweight nature of Phaser and the relative ease of entry (extremely if you compare it to Unreal)
