// Darren Hobern 2019

export class Paddle extends Phaser.Sprite {

  moveSpeed:number = 100;
  hFactor:number = 100;

  constructor(game:Phaser.Game, x:number, y:number) {
    super(game, x, y, 'paddleSprite');
    game.add.existing(this);
    game.physics.enable(this, Phaser.Physics.ARCADE);
    this.body.fixedRotation = true;
    this.body.collideWorldBounds = true;
    this.body.immovable = true;
  }

  /** Sets the paddle's horizontal velocity moving left.*/
  moveLeft(): void {
    this.body.velocity.x = -this.moveSpeed;
  }

  /** Sets the paddle's horizontal velocity moving right.*/
  moveRight():void {
    this.body.velocity.x = this.moveSpeed;
  }

  /** Stops the paddle. */
  stop():void {
    this.body.velocity.x = 0;
  }

  collisionHandler(paddle, ball):void {
    ball.body.velocity.x += ((ball.centerX-paddle.body.x)/paddle.body.width - 0.5) * paddle.hFactor;
  }
}
