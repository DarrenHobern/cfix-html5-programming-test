// Darren Hobern 2019

import {Breakout} from './Breakout';

export class Ball extends Phaser.Sprite {

  constructor(game:Breakout, x:number, y:number, xVelocity?: number, yVelocity?: number) {
    super(game, x, y, 'ballSprite');
    game.add.existing(this);
    game.physics.enable(this, Phaser.Physics.ARCADE);
    this.body.bounce.set(1);
    this.body.stopVelocityOnCollide = false;
    this.body.fixedRotation = true;
    this.body.collideWorldBounds = true;
    this.body.velocity.setTo(xVelocity, yVelocity);
    this.checkWorldBounds = true;
  }

}
