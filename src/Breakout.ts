// Darren Hobern 2019

import {Paddle} from './Paddle';
import {Box} from './Box';
import {Ball} from './Ball';

export class Breakout extends Phaser.Game {

  paddle: Paddle;
  left: boolean = false;
  right: boolean = false;
  floorOffset: number = 10;

  boxGroup: Phaser.Group;
  rows: number = 3;
  columns: number = 10;

  ball: Ball;

  buttonLeft: Phaser.Button;
  buttonRight: Phaser.Button;

  scaleRatio:number;

  constructor() {
    super(window.innerWidth * window.devicePixelRatio,
      window.innerHeight * window.devicePixelRatio,
      Phaser.CANVAS, '', {preload: preload, create: create, update: update});
    console.log("Hello, World! from Phaser-" + Phaser.VERSION);

    function preload() {
      this.scaleRatio = window.devicePixelRatio / 2; // highest DPR I'll support is 2
      this.load.image('paddleSprite', 'assets/paddle.png');
      this.load.image('ballSprite', 'assets/ball.png');
      this.load.image('box1Sprite', 'assets/boxEmpty.png');
      this.load.image('box2Sprite', 'assets/box.png');
      this.load.image('box3Sprite', 'assets/boxAlt.png');
      this.load.image('buttonLeftSprite', 'assets/buttonLeft.png');
      this.load.image('buttonRightSprite', 'assets/buttonRight.png');
      this.load.image('buttonRestartSprite', 'assets/buttonRestart.png');
    }

    function create() {
      this.stage.backgroundColor = '00ffff';
      this.physics.startSystem(Phaser.Physics.ARCADE);
      this.physics.arcade.checkCollision.down = false;

      // Paddle
      var paddleImage:HTMLImageElement = this.cache.getImage('paddleSprite');
      this.floorOffset = 1.2;
      this.paddle = new Paddle(this, this.world.centerX - paddleImage.width/2,
                              (this.world.height - paddleImage.height-this.floorOffset));

      // Boxes
      this.boxGroup = this.add.physicsGroup();
      var boxImage:HTMLImageElement = this.cache.getImage('box1Sprite');
      // TODO determine why class fields' default values aren't initialised
      this.rows = 2
      this.columns = Phaser.Math.floorTo(this.world.width/boxImage.width, 1,10)-1;

      for (var r = 0; r < this.rows; r++) {
        for (var c = 0; c < this.columns; c++) {
          this.boxGroup.add(new Box(this, c*boxImage.width, r*boxImage.height))
        }
      }
      this.boxGroup.x = (this.world.width - (this.columns * boxImage.width))/2;
      this.boxGroup.setAll('body.immovable', true);
      this.boxGroup.callAll('scale.setTo', this.scaleRatio, this.scaleRatio);

      // Ball
      var ballImage:HTMLImageElement = this.cache.getImage('ballSprite');
      this.ball = new Ball(this, this.world.centerX,
        this.paddle.y-ballImage.height, 0, -150);
      this.ball.events.onOutOfBounds.add(gameOver, this);

      // Buttons
      var buttonImage:HTMLImageElement = this.cache.getImage('buttonRightSprite');
      // Left Button:
      this.buttonLeft = this.add.button(0, this.world.height*3/4,
                                        'buttonLeftSprite', null, this);
      this.buttonLeft.inputEnabled = true;
      this.buttonLeft.fixedToCamera = true;
      this.buttonLeft.events.onInputOver.add(() => {this.left=true;});
      this.buttonLeft.events.onInputOut.add(() => {this.left=false;});
      this.buttonLeft.events.onInputDown.add(() => {this.left=true;});
      this.buttonLeft.events.onInputUp.add(() => {this.left=false;});
      // Right Button:
      this.buttonRight = this.add.button(this.world.width - buttonImage.width,
                                        this.world.height*3/4,
                                        'buttonRightSprite', null, this);
      this.buttonRight.inputEnabled = true;
      this.buttonRight.fixedToCamera = true;
      this.buttonRight.events.onInputOver.add(() => {this.right=true;});
      this.buttonRight.events.onInputOut.add(() => {this.right=false;});
      this.buttonRight.events.onInputDown.add(() => {this.right=true;});
      this.buttonRight.events.onInputUp.add(() => {this.right=false;});

      // Restart Button:
      this.restartButton = this.add.button(this.world.centerX, this.world.centerY,
                                          'buttonRestartSprite', null, this);
      this.restartButton.inputEnabled = true;
      this.restartButton.fixedToCamera = true;
      this.restartButton.events.onInputDown.add(restartGame, this);
      this.restartButton.kill();
    }

    function update() {
      this.physics.arcade.collide(this.ball, this.boxGroup, collisionHandler)
      this.physics.arcade.collide(this.paddle, this.ball, this.paddle.collisionHandler);

      if (this.left) {
        this.paddle.moveLeft();
      }
      else if (this.right) {
        this.paddle.moveRight();
      }
      else {
        this.paddle.stop();
      }
    }

    function collisionHandler(ball, box) {
      box.damage(1);
    }

    function gameOver():void {
      // TODO show the restart button
      this.restartButton.revive(1);
      this.paused = true;
    }

    function restartGame():void {
      this.restartButton.kill();
      this.paused = false;
      this.boxGroup.callAll('revive', null, 1);
      this.ball.x = this.world.centerX;
      this.ball.y = this.world.height*3/4;
      this.ball.body.velocity.setTo(0, 150);
      this.paddle.x = this.world.centerX;
      this.paddle.y = this.world.height - this.paddle.height - this.floorOffset;
      this.paddle.stop();

    }
  }

}
